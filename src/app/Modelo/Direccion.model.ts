export class DireccionModel {
  constructor(
    public codigo: string,
    public calle: string,
    public ciudad: string,
    public codigoPostal: string,
    public estado: string,
    public pais: string
  ) { }

  static fromJson(obj: any) {

    return new DireccionModel(
      obj.codigo,
      obj.calle,
      obj.ciudad,
      obj.codigoPostal,
      obj.estado,
      obj.pais
    )
  }
}



