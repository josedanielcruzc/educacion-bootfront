import { DireccionModel } from './Direccion.model';

export class PersonaModel {
  constructor(
    public codigo: string,
    public mail: string,
    public nombre: string,
    public telefono: string,
    public direccionTO: DireccionModel
  ) { }

  static fromJson(obj: any) {

    return new PersonaModel(
      obj.codigo,
      obj.mail,
      obj.nombre,
      obj.telefono,
      obj.direccionTO
    )
  }
}



