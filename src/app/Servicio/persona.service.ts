import { Injectable } from '@angular/core';

import { HttpClient, HttpHeaders } from '@angular/common/http';
import { map } from 'rxjs/operators';
import { environment } from '../../environments/environment';
import { PersonaModel } from '../Modelo/Persona.model';

@Injectable({
  providedIn: 'root'
})
export class PersonaService {

  constructor(private http: HttpClient) { }

  public httpOptions = {
    headers: new HttpHeaders({ 'Content-Type': 'application/json' })
  };

  public getPersonasAPI(): Promise<PersonaModel[]> {
    return this.http.get(`${environment.api_endpoint}/persona`, this.httpOptions)
      .pipe(
        map((res: any[]) => {
          const listaPersonas: PersonaModel[] = [];
          res.forEach(persona => {
            listaPersonas.push(PersonaModel.fromJson(persona));
          });
          return listaPersonas;
        }
        )
      ).toPromise();
  }



  public guardarPersonaAPI(personaModel: PersonaModel): Promise<PersonaModel> {

    const body = personaModel;

    return this.http.post(`${environment.api_endpoint}/persona`, body, this.httpOptions)
      .pipe(
        map((res: any) => {
          console.log('Desde el service post: ', res);
          return PersonaModel.fromJson(res);
        }
        )
      ).toPromise();
  }

  public actualizarPersonaAPI(personaModel: PersonaModel): Promise<PersonaModel> {
    const body = personaModel;
    return this.http.post(`${environment.api_endpoint}/persona/actualizar`, body, this.httpOptions)
      .pipe(
        map((res: any) => {
          console.log('Desde el service put: ', res);
          return PersonaModel.fromJson(res);
        }
        )
      ).toPromise();
  }

  
  public deletePersonaAPI(personaModel: PersonaModel): Promise<PersonaModel> {
    const body = personaModel;

    return this.http.post(`${environment.api_endpoint}/persona/eliminar`, body, this.httpOptions )
      .pipe(
        map((res: any) => {
          console.log('Desde el service delete: ', res);
          return res;
        }
        )
      ).toPromise();
  }

}
