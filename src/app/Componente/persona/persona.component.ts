import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { MatSnackBar } from '@angular/material';
import { PersonaService } from 'src/app/Servicio/persona.service';
import { PersonaModel } from 'src/app/Modelo/Persona.model';
import { DireccionModel } from 'src/app/Modelo/Direccion.model';
import { ErrorModel } from 'src/app/Modelo/ErrorModel';


@Component({
  selector: 'app-persona',
  templateUrl: './persona.component.html',
  styleUrls: ['./persona.component.css']
})
export class PersonaComponent implements OnInit {

  public listaPersonas: PersonaModel[] = [];

  registerForm: FormGroup;
  submitted = false;

  public personaSeleccionada: PersonaModel;

  public mostrarTarjetaGuardar = false;
  public mostrarCamposDireccion = false;

  constructor(
    private router: Router,
    private snackbarCtrl: MatSnackBar,
    private personaCtrl: PersonaService,
    private formBuilder: FormBuilder
  ) { }

  ngOnInit() {
    this.getPersonas();
    this.registerForm = this.formBuilder.group({
      mail: ['', Validators.required],
      nombre: ['', Validators.required],
      telefono: ['', Validators.required],

      conDireccion: ['', null],

      calle: ['', Validators.required],
      ciudad: ['', Validators.required],
      codigoPostal: ['', null],
      estado: ['', Validators.required],
      pais: ['', Validators.required]
    });
  }



  guardarPersona(event: any) {
    this.submitted = true;
    if (this.registerForm.invalid) {
      console.log('fallo');
      return;
    }
    const persona = this.registerForm.value;
    console.log("persona seleccionada guardar: ", this.personaSeleccionada);
    const personaModel = new PersonaModel(
      this.personaSeleccionada != null ? this.personaSeleccionada.codigo : null,
      persona.mail,
      persona.nombre,
      persona.telefono,
      persona.conDireccion != false ? new DireccionModel(
        this.personaSeleccionada.direccionTO != null ? this.personaSeleccionada.direccionTO.codigo : null,
        persona.calle,
        persona.ciudad,
        persona.codigoPostal,
        persona.estado,
        persona.pais
      ) : null
    )
    console.log("Modelo Guardar", personaModel);

    if (this.personaSeleccionada != null) {
      this.personaCtrl.actualizarPersonaAPI(personaModel).then(res => {
        this.personaSeleccionada = null;
        this.mostrarTarjetaGuardar = !this.mostrarTarjetaGuardar;
        this.snackbarCtrl.open(`Persona actualizada con éxito`, 'Entendido')
          .onAction().subscribe(() => {
            this.resetFormulario();
            this.getPersonas();
          });
      }, error => {
        if (error instanceof ErrorModel) {
          this.snackbarCtrl.open(`${error.getMessage()}`, 'Aceptar');
          this.router.navigate(['/']);
        } else if (error.status === 403 || error.status === 500 || error.status === 0) {
          const msg = ErrorModel.getErrorAuthentication(error.status);
          this.snackbarCtrl.open(`${msg}`, 'Aceptar');
        } else {
          this.snackbarCtrl.open(`${error.error.errorMessage}`, 'Aceptar');
        }
      });
    } else {
      this.personaCtrl.guardarPersonaAPI(personaModel).then(res => {
        this.personaSeleccionada = null;
        this.mostrarTarjetaGuardar = !this.mostrarTarjetaGuardar;
        this.snackbarCtrl.open(`Persona guardada con éxito`, 'Entendido')
          .onAction().subscribe(() => {
            this.resetFormulario();
            this.getPersonas();
          });
      }, error => {
        if (error instanceof ErrorModel) {
          this.snackbarCtrl.open(`${error.getMessage()}`, 'Aceptar');
          this.router.navigate(['/']);
        } else if (error.status === 403 || error.status === 500 || error.status === 0) {
          const msg = ErrorModel.getErrorAuthentication(error.status);
          this.snackbarCtrl.open(`${msg}`, 'Aceptar');
        } else {
          this.snackbarCtrl.open(`${error.error.errorMessage}`, 'Aceptar');
        }
      });
    }




  }

  public personaSeleccionar(event: any, persona: any, accion: string) {
    console.log("persona seleccionada 1: ", persona);
    this.personaSeleccionada = persona;
    console.log("persona seleccionada 2: ", this.personaSeleccionada);
    if (accion === 'editar') {
      this.registerForm.setValue({
        mail: persona.mail,
        nombre: persona.nombre,
        telefono: persona.telefono,

        conDireccion: persona.direccionTO != null,
        calle: persona.direccionTO != null ? persona.direccionTO.calle : null,
        ciudad: persona.direccionTO != null ? persona.direccionTO.ciudad : null,
        codigoPostal: persona.direccionTO != null ? persona.direccionTO.codigoPostal : null,
        estado: persona.direccionTO != null ? persona.direccionTO.estado : null,
        pais: persona.direccionTO != null ? persona.direccionTO.pais : null
      }
      );
      this.renderizarControles(persona.direccionTO != null);
      this.mostrarTarjetaGuardar = !this.mostrarTarjetaGuardar;
    } else if (accion === 'eliminar') {
      this.snackbarCtrl.open(`Seguro de eliminar`, 'Entendido', { duration: 3000 })
        .onAction().subscribe(() => {
          this.eliminarPersona(persona)
        });
    }
  }


  public eliminarPersona(personaModel: PersonaModel) {
    this.personaCtrl.deletePersonaAPI(personaModel).then(res => {
      this.snackbarCtrl.open('Persona eliminada correctamente', 'Entendido');
      this.getPersonas();
    }, error => {
      if (error instanceof ErrorModel) {
        this.snackbarCtrl.open(`${error.getMessage()}`, 'Aceptar');
      } else if (error.status === 403 || error.status === 500 || error.status === 0) {
        const msg = ErrorModel.getErrorAuthentication(error.status);
        this.snackbarCtrl.open(`${msg}`, 'Aceptar');
      } else {
        this.snackbarCtrl.open(`${error.error.errorMessage}`, 'Aceptar');
      }
    });
    console.log("Eliminar : ", personaModel);
  }

  public cancelarGuardado() {
    this.mostrarTarjetaGuardar = !this.mostrarTarjetaGuardar;
    this.resetFormulario();
  }

  public agregarPersona() {
    this.resetFormulario();
    this.renderizarControles(false);
    this.mostrarTarjetaGuardar = !this.mostrarTarjetaGuardar;
  }

  public getPersonas() {
    this.personaCtrl.getPersonasAPI()
      .then(res => {
        this.listaPersonas = res;
        console.log("personas :", this.listaPersonas);
      }, error => {
        if (error instanceof ErrorModel) {
          this.snackbarCtrl.open(`${error.getMessage()}`, 'Aceptar');
          /* this.router.navigate(['/']); */
        } else if (error.status === 403 || error.status === 500 || error.status === 0) {
          const msg = ErrorModel.getErrorAuthentication(error.status);
          this.snackbarCtrl.open(`${msg}`, 'Aceptar');
        } else {
          this.snackbarCtrl.open(`${error.error.errorMessage}`, 'Aceptar');
        }
      });
  }

  public cambioConDireccion(newValue) {
    console.log('nuevo valor ', newValue);
    this.renderizarControles(newValue);
  }

  public resetFormulario() {
    this.registerForm.get("mail").setValue("");
    this.registerForm.get("nombre").setValue("");
    this.registerForm.get("telefono").setValue("");

    this.registerForm.get("conDireccion").setValue(false);
    this.renderizarControles(false);
  }

  public renderizarControles(valor) {
    if (valor == true) {
      this.registerForm.get('calle').setValidators(Validators.required);
      this.registerForm.get('calle').updateValueAndValidity();
      this.registerForm.get('ciudad').setValidators(Validators.required);
      this.registerForm.get('ciudad').updateValueAndValidity();
      this.registerForm.get('codigoPostal').setValidators(null);
      this.registerForm.get('codigoPostal').updateValueAndValidity();
      this.registerForm.get('estado').setValidators(Validators.required);
      this.registerForm.get('estado').updateValueAndValidity();
      this.registerForm.get('pais').setValidators(Validators.required);
      this.registerForm.get('pais').updateValueAndValidity();
      this.mostrarCamposDireccion = true;
    } else {
      this.registerForm.get('calle').clearValidators();
      this.registerForm.get('calle').updateValueAndValidity();
      this.registerForm.get('ciudad').clearValidators();
      this.registerForm.get('ciudad').updateValueAndValidity();
      this.registerForm.get('codigoPostal').clearValidators();
      this.registerForm.get('codigoPostal').updateValueAndValidity();
      this.registerForm.get('estado').clearValidators();
      this.registerForm.get('estado').updateValueAndValidity();
      this.registerForm.get('pais').clearValidators();
      this.registerForm.get('pais').updateValueAndValidity();

      this.registerForm.get("calle").setValue("");
      this.registerForm.get("ciudad").setValue("");
      this.registerForm.get("codigoPostal").setValue("");
      this.registerForm.get("estado").setValue("");
      this.registerForm.get("pais").setValue("");
      this.mostrarCamposDireccion = false;
    }
  }

  get f() { return this.registerForm.controls; }

}
